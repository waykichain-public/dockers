cd /opt/docker-instances/mainnet/data
sudo wget http://waykichain.oss-cn-shenzhen.aliyuncs.com/native/coind-20190223.tar.gz
docker exec -t  waykicoind-main sh -c "mv ~/.WaykiChain/main/coind-20190223.tar.gz ."
docker exec -t  waykicoind-main sh -c "rm -rf ./coind"
docker exec -t  waykicoind-main sh -c "tar xzvf ./coind-20190223.tar.gz"
docker exec -t  waykicoind-main sh -c "coind stop && echo 'sleep 5 sec...' && sleep 2"
sleep 3
docker start waykicoind-main