VER=$1
if [ -z $VER ]; then VER=1.0; fi

docker build -t waykichain/wcoind:$VER .