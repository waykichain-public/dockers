docker tag 491f6e38d55e wicc/waykicoind:v1.1.1
docker pull wicc/waykicoind
docker tag fc0f336c66ff wicc/waykicoind:v1.2.0

docker stop waykicoind-main-temp && docker rm waykicoind-main-temp

cd /opt/docker-instances/mainnet-temp \
&& docker run --name waykicoind-main-temp \
       -v `pwd`/conf/WaykiChain.conf:/root/.WaykiChain/WaykiChain.conf \
       -v `pwd`/data:/root/.WaykiChain/main \
       -v `pwd`/bin:/opt/wicc/bin \
       -d wicc/waykicoind:v1.2.5