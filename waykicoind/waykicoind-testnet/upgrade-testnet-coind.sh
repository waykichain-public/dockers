BIN_DIR='/opt/docker-instances/testnet/bin'
CON_NAME='waykicoind-testnet'

COIND_FILE=$1
COIND_URL="http://waykichain.oss-cn-shenzhen.aliyuncs.com/native/$COIND_FILE"

docker stop $CON_NAME

wget -O /tmp/$COIND_FILE $COIND_URL
cd $BIN_DIR && sudo rm -rf ./coind && sudo tar xzvf /tmp/$COIND_FILE

docker start $CON_NAME