cd /opt/docker-instances/testnet
docker pull wicc/waykicoind
docker stop waykicoind-test && docker rm waykicoind-test

docker pull wicc/waykicoind
docker tag wicc/waykicoind:latest wicc/waykicoind:v1.2.0

cd /opt/docker-instances/testnet \
&& docker run --name waykicoind-testnet -p 18920:18920 -p 6967:6968 \
       -v `pwd`/conf/WaykiChain.conf:/root/.WaykiChain/WaykiChain.conf \
       -v `pwd`/data:/root/.WaykiChain/main \
       -v `pwd`/bin:/opt/wicc/bin \
       -d wicc/waykicoind:v1.2.0