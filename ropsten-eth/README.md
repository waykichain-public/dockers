## geth attach

In case ```geth attach``` not working due to failing to locate ```geth.ipc``` file, run the following:

```geth attach http://localhost:8545```